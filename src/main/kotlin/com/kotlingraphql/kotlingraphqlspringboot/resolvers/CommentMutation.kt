package com.kotlingraphql.kotlingraphqlspringboot.resolvers

import com.expediagroup.graphql.spring.operations.Mutation
import com.kotlingraphql.kotlingraphqlspringboot.entities.Comment
import com.kotlingraphql.kotlingraphqlspringboot.repositories.CommentRepositoryImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class CommentMutation : Mutation {
    @Autowired
    lateinit var commentRepositoryImpl: CommentRepositoryImpl

    fun createComment(content: String, bookId: String, userId: String): Comment {
        return commentRepositoryImpl.createComment(content, bookId, userId)
    }

}