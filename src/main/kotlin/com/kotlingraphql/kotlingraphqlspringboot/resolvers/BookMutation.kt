package com.kotlingraphql.kotlingraphqlspringboot.resolvers


import com.expediagroup.graphql.spring.operations.Mutation
import com.kotlingraphql.kotlingraphqlspringboot.entities.Book
import com.kotlingraphql.kotlingraphqlspringboot.repositories.BookRepositoryImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class BookMutation: Mutation {
    @Autowired
    lateinit var bookRepositoryImpl: BookRepositoryImpl

    fun createNewBook(name: String, publishDate: String, userId: String): Book {
        return bookRepositoryImpl.createBook(name = name, publishDate = publishDate, userId = userId )
    }
}