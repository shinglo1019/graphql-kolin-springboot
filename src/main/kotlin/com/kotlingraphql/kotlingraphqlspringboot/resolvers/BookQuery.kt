package com.kotlingraphql.kotlingraphqlspringboot.resolvers

import com.expediagroup.graphql.spring.operations.Query
import com.kotlingraphql.kotlingraphqlspringboot.entities.Book
import com.kotlingraphql.kotlingraphqlspringboot.repositories.BookRepository
import com.kotlingraphql.kotlingraphqlspringboot.repositories.BookRepositoryImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class BookQuery: Query{
    @Autowired
    lateinit var bookRepositoryImpl: BookRepositoryImpl

    fun getBookById(id: String): Book {
        return bookRepositoryImpl.getBookById(id)
    }

    fun getAllBook(offset: Long, count: Int, filter: String?, sortOrder: String?, sortBy: String?): List<Book> {
        return bookRepositoryImpl.getAllBook(offset, count, filter, sortOrder, sortBy)
    }
}
