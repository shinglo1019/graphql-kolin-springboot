package com.kotlingraphql.kotlingraphqlspringboot.resolvers


import com.expediagroup.graphql.spring.operations.Mutation
import com.kotlingraphql.kotlingraphqlspringboot.entities.User
import com.kotlingraphql.kotlingraphqlspringboot.repositories.UserRepositoryImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class UserMutation : Mutation {
    @Autowired
    lateinit var userRepositoryImpl: UserRepositoryImpl

    fun createUser(name: String, avatar: String): User {
        return userRepositoryImpl.createUser(name = name, avatar = avatar)
    }

}