package com.kotlingraphql.kotlingraphqlspringboot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinGraphqlSpringbootApplication

fun main(args: Array<String>) {
	runApplication<KotlinGraphqlSpringbootApplication>(*args)
}
