package com.kotlingraphql.kotlingraphqlspringboot.repositories

import com.kotlingraphql.kotlingraphqlspringboot.entities.User
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository: MongoRepository<User, String>