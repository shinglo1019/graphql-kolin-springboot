package com.kotlingraphql.kotlingraphqlspringboot.repositories

import com.kotlingraphql.kotlingraphqlspringboot.entities.Book
import com.kotlingraphql.kotlingraphqlspringboot.entities.User
import com.kotlingraphql.kotlingraphqlspringboot.entities.Comment
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.aggregation.LookupOperation
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.stereotype.Component
import org.springframework.data.mongodb.core.query.Query

@Component
class BookRepositoryImpl(private val mongoOperations: MongoOperations) {
    @Autowired
    lateinit var bookRepository: BookRepository

    fun getAllBook(offset: Long, count: Int, filter: String?, sortOrder: String?, sortBy: String? ): List<Book> {
        val query = Query()
        val sorting = if (sortOrder == "desc") {
            Sort.Direction.DESC
        } else {
            Sort.Direction.ASC
        }
        filter?.let {
            query.addCriteria(
                    Criteria().orOperator(
                            Criteria.where("name").regex(it),
                            Criteria.where("author.name").regex(it)))
        }
        sortBy?.let {
            println(sorting)
            println(sortBy)
            query.with(Sort.by(sorting, sortBy))
        }

        query.skip(offset).limit(count)
        val books = mongoOperations.find(query, Book::class.java)

        for (item in books) {
            item.author = getUser(item.userId)

            if (!item.commentsId.isNullOrEmpty()) {
                item.comments = getComment(item._id)
            }else{
                item.comments = emptyList()
            }
        }
        return books
    }

    fun getBookById(id: String): Book {
        val query = Query()
        query.addCriteria(Criteria.where("_id").`is`(id))
        val book = mongoOperations.find(query, Book::class.java)[0]

        book.author = getUser(book.userId)
        book.comments = getComment(book._id)

        return book
//        val lookup = LookupOperation.newLookup().from("users").localField("userId").foreignField("_id").`as`("users")
//        val match = Aggregation.match(Criteria.where("_id").`is`(id))
////        var list = ArrayList<DBObject>()
////        list.add(lookup.toDBObject(Aggregation.DEFAULT_CONTEXT))
//        val aggregation = Aggregation.newAggregation(Book::class.java, match, lookup)
//
//        val result = mongoOperations.aggregate(aggregation, Book::class.java).mappedResults
//        result[0].author = userRepository.findById(result[0].userId.toHexString()).get()
//        return result
    }

    fun createBook(name: String, publishDate: String, userId: String): Book {
        val _id = ObjectId().toHexString()
        val book = Book(_id = _id, name = name, publishDate = publishDate, userId = userId, commentsId = emptyList())
        bookRepository.save(book)
        return book
    }

    fun getUser(id: String): User {
        val query = Query()
        query.addCriteria(Criteria.where("_id").`is`(id))
        return mongoOperations.find(query, User::class.java)[0]
    }

    fun getComment(id: String): List<Comment> {
        val query = Query()
        query.addCriteria(Criteria.where("bookId").`is`(id))
        val comments = mongoOperations.find(query, Comment::class.java)

        for(item in comments){
            item.author = getUser(item.userId)
        }

        return comments
    }

}