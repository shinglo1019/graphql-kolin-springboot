package com.kotlingraphql.kotlingraphqlspringboot.repositories

import com.kotlingraphql.kotlingraphqlspringboot.entities.Book
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface BookRepository: MongoRepository<Book, String>

