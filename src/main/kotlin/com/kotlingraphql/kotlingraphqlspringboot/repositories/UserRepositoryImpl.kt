package com.kotlingraphql.kotlingraphqlspringboot.repositories

import com.kotlingraphql.kotlingraphqlspringboot.entities.User
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.stereotype.Component
import org.springframework.data.mongodb.core.query.Query
import java.util.*

@Component
class UserRepositoryImpl(private val mongoOperations: MongoOperations) {
    @Autowired
    lateinit var repository: UserRepository

//    fun getBookById(id: String): List<Book> {
//        val query = Query()
//        query.addCriteria(Criteria.where("id").`is`(id))
//        return mongoOperations.find(query, Book::class.java)
//    }

    fun createUser(name: String, avatar: String): User {
        val _id = ObjectId().toHexString()
        val user = User(_id = _id, name = name, avatar = avatar)
        repository.save(user)
        return user
    }

}