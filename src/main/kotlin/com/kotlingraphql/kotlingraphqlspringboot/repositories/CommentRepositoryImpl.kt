package com.kotlingraphql.kotlingraphqlspringboot.repositories

import com.kotlingraphql.kotlingraphqlspringboot.entities.Book
import com.kotlingraphql.kotlingraphqlspringboot.entities.Comment
import com.kotlingraphql.kotlingraphqlspringboot.entities.User
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.stereotype.Component
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update

@Component
class CommentRepositoryImpl(private val mongoOperations: MongoOperations) {
    @Autowired
    lateinit var commentRepository: CommentRepository
    lateinit var bookRepository: BookRepository


    fun createComment(content: String, bookId: String, userId: String): Comment {
        val _id = ObjectId().toHexString()
        val comment = Comment(_id, content, bookId, userId)
        commentRepository.save(comment)

        pushBookComment(bookId = bookId, commentId = comment._id)
        return comment
    }

    fun pushBookComment(bookId: String, commentId: String): Boolean {
        val query = Query()
        query.addCriteria(Criteria.where("_id").`is`(bookId))

        val update = Update()
        update.addToSet("commentsId", commentId)
        mongoOperations.updateFirst(query, update, Book::class.java)
        return true
    }
}