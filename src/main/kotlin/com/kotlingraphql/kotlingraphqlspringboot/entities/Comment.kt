package com.kotlingraphql.kotlingraphqlspringboot.entities

import com.kotlingraphql.kotlingraphqlspringboot.annotations.GraphQLIgnore
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "comments")
data class Comment(
        val _id: String,
        val content: String,
        val bookId: String,
        @GraphQLIgnore
        var userId: String
){
    lateinit var author: User
}