package com.kotlingraphql.kotlingraphqlspringboot.entities

import org.springframework.data.mongodb.core.mapping.Document
import com.kotlingraphql.kotlingraphqlspringboot.annotations.GraphQLIgnore

@Document(collection = "books")
data class Book(
        val _id: String,
        val name: String,
        val publishDate: String,
        @GraphQLIgnore
        var userId: String,
        var commentsId: List<String>?
) {
    lateinit var author: User
    lateinit var comments: List<Comment>
}