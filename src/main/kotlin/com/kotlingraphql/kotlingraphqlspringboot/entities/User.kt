package com.kotlingraphql.kotlingraphqlspringboot.entities

import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "users")
data class User(
        val _id: String,
        val name: String,
        val avatar: String
)