package com.kotlingraphql.kotlingraphqlspringboot.resolvers

import com.kotlingraphql.kotlingraphqlspringboot.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest
@AutoConfigureWebTestClient
@TestInstance(PER_CLASS)
class BookQueryTest(@Autowired private val testClient: WebTestClient) {


    @Test
    fun `verify getBookById query`() {
        val query = "getBookById"
//        val expectedData = "false"

        testClient.post()
                .uri(GRAPHQL_ENDPOINT)
                .accept(APPLICATION_JSON)
                .contentType(GRAPHQL_MEDIA_TYPE)
                .bodyValue("query { $query(id: \"5feaae9db7518b15832d678c\"){ _id, name} }")
                .exchange()
//                .verifyData(query, "true")
                .verifyOnlyDataExists(query)
                .jsonPath("$DATA_JSON_PATH.$query.name").isEqualTo("New Books")
    }

    @Test
    fun `verify getAllBook query`() {
        val query = "getAllBook"
//        val expectedData = "false"

        testClient.post()
                .uri(GRAPHQL_ENDPOINT)
                .accept(APPLICATION_JSON)
                .contentType(GRAPHQL_MEDIA_TYPE)
                .bodyValue("query { $query(offset:0, count:9) { _id, name}}")
                .exchange()
                .verifyOnlyDataExists(query)
                .jsonPath("$DATA_JSON_PATH.$query").isArray

    }





//    @Test
//    fun getBookById() {
//
////
//////        val test = mongoTemplate?.save(book, "books")
//////        val bookRepository = context?.getBean(BookRepository::class.java)
//////        doReturn(book).`when`(bookRepositoryImpl.getBookById("5fe1cef9630ae821dc4972de"))
////        val newBook = bookRepositoryImpl.createBook("abc", "2020-03-29", "321")
////
////        val newBook2 = bookRepositoryImpl.getBookById("5fe1cef9630ae821dc4972de")
////
////
//////        val abc = bookRepositoryImpl.getBookById(newBook._id)
////        val response: GraphQLResponse = graphQLTestTemplate.postForResource("getBookById.graphql")
////        assertNotNull(response)
////        assertThat(response.isOk).isTrue()
////        assertThat(response.get("$.data.getBookById.name")).isEqualTo("New Book")
//////    given(bookRepositoryImpl.getBookById("5fe1ce66ef67421b436a5ce6")).willReturn(Book("5fe1ce66ef67421b436a5ce6","New Book", "2020-12-22","5fde18ad118deab27153bb10", emptyList()))
//
//    }

}