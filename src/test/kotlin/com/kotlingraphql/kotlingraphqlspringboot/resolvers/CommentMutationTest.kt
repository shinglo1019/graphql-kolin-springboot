package com.kotlingraphql.kotlingraphqlspringboot.resolvers

import com.kotlingraphql.kotlingraphqlspringboot.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest
@AutoConfigureWebTestClient
@TestInstance(PER_CLASS)

internal class CommentMutationTest(@Autowired private val testClient: WebTestClient) {


    @Test
    fun `verity createComment mutation`() {
        val mutation = "createComment"

        testClient.post()
                .uri(GRAPHQL_ENDPOINT)
                .accept(APPLICATION_JSON)
                .contentType(GRAPHQL_MEDIA_TYPE)
                .bodyValue("mutation { $mutation(content:\"Nice Book!\", bookId:\"5fe1cef9630ae821dc4972de\", userId:\"5fe1c550e89c304d655bea5c\") { _id, content } }")
                .exchange()
                .verifyOnlyDataExists(mutation)
//                .jsonPath("$DATA_JSON_PATH.$query._id").isEqualTo(1)
                .jsonPath("$DATA_JSON_PATH.$mutation.content").isEqualTo("Nice Book!")

    }
}