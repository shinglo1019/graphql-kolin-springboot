package com.kotlingraphql.kotlingraphqlspringboot.resolvers

import com.kotlingraphql.kotlingraphqlspringboot.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest
@AutoConfigureWebTestClient
@TestInstance(PER_CLASS)
internal class UserMutationTest(@Autowired private val testClient: WebTestClient) {

    @Test
    fun `verity createUser mutation`() {
        val mutation = "createUser"

        testClient.post()
                .uri(GRAPHQL_ENDPOINT)
                .accept(APPLICATION_JSON)
                .contentType(GRAPHQL_MEDIA_TYPE)
                .bodyValue("mutation { $mutation(name:\"Shing Lo\", avatar:\"\") { _id, name, avatar } }")
                .exchange()
                .verifyOnlyDataExists(mutation)
                .jsonPath("$DATA_JSON_PATH.$mutation.name").isEqualTo("Shing Lo")
                .jsonPath("$DATA_JSON_PATH.$mutation.avatar").isEqualTo("")

    }
}