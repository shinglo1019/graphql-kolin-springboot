package com.kotlingraphql.kotlingraphqlspringboot.resolvers

import com.kotlingraphql.kotlingraphqlspringboot.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest
@AutoConfigureWebTestClient
@TestInstance(PER_CLASS)
internal class BookMutationTest(@Autowired private val testClient: WebTestClient) {

    @Test
    fun `verity createNewBook mutation`() {
        val mutation = "createNewBook"

        testClient.post()
                .uri(GRAPHQL_ENDPOINT)
                .accept(APPLICATION_JSON)
                .contentType(GRAPHQL_MEDIA_TYPE)
                .bodyValue("mutation { $mutation(name: \"New Books\", publishDate: \"2020-12-22\", userId: \"5fde18ad118deab27153bb10\") { _id, name } }")
                .exchange()
                .verifyOnlyDataExists(mutation)
//                .jsonPath("$DATA_JSON_PATH.$query._id").isEqualTo(1)
                .jsonPath("$DATA_JSON_PATH.$mutation.name").isEqualTo("New Books")

    }
}