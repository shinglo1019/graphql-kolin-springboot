import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.4.1"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    kotlin("jvm") version "1.4.21"
    kotlin("plugin.spring") version "1.4.21"
}

group = "com.kotlingraphql"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.expediagroup:graphql-kotlin-spring-server:2.1.0")
    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    testImplementation("org.springframework.boot:spring-boot-starter-test")


//    implementation( "com.graphql-java:graphql-spring-boot-starter:5.0.2")
//    implementation( "com.graphql-java:graphiql-spring-boot-starter:5.0.2")
//    implementation( "com.graphql-java:graphql-java-tools:5.2.4")

//    testImplementation("com.graphql-java-kickstart:graphql-spring-boot-starter-test:8.1.0")
//    testImplementation ("org.springframework:spring-test:5.3.2")
//    testImplementation ("junit:junit:4.12")
//    testImplementation("org.springframework.boot:spring-boot-starter-test")
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
    implementation("org.springdoc:springdoc-openapi-webflux-ui:1.4.3")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude( group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("io.projectreactor:reactor-test:3.4.1")

}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
allOpen {
    annotation("javax.persistence.Entity")
    annotation("javax.persistence.Embeddable")
    annotation("javax.persistence.MappedSuperclass")
}
tasks.withType<Test> {
    useJUnitPlatform()
}
